lua require('init')

filetype plugin indent on

" TODO: Convert into lua
let g:fzf_action = {
    \ 'ctrl-h': 'split',
    \ 'ctrl-v': 'vsplit' 
    \ }

let g:user_emmmet_mode='a'
let g:user_emmet_expandabbr_key = '<C-p>'

inoremap öö <Esc>

hi normal guibg=none
hi LineNr guifg=Yellow
hi CursorLineNr guifg=Yellow
hi StatusLine guifg=Grey guibg=White
