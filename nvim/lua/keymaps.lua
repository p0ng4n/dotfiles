local keymap = vim.api.nvim_set_keymap

-- Set space as leader key
keymap('n', '<Space>', '', {})
vim.g.mapleader = ' '

-- Better movement between panes
keymap('n', '<c-h>', '<c-w>h', { noremap = true }) -- Left
keymap('n', '<c-j>', '<c-w>j', { noremap = true }) -- Down
keymap('n', '<c-k>', '<c-w>k', { noremap = true }) -- Up
keymap('n', '<c-l>', '<c-w>l', { noremap = true }) -- Right

-- Better movement between tabs
keymap('n', '<leader>j', ':tabprevious<CR>', { noremap = true })
keymap('n', '<leader>k', ':tabnext<CR>', { noremap = true })

-- Open another file
keymap('n', '<F2>', ':NERDTreeToggle<CR>', { noremap = true })
keymap('n', '<c-f>',
    "<cmd>lua require('fzf-lua').files()<CR>",
    { noremap = true, silent = true })

-- Source init.vim
keymap('n', '<leader>r', ':source ~/.config/nvim/init.vim<CR>', { noremap = true })
