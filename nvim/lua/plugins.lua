
vim.cmd([[packadd packer.nvim]])


return require('packer').startup(function()

use 'wbthomason/packer.nvim'
use 'preservim/nerdtree'
use 'jiangmiao/auto-pairs'
use 'sheerun/vim-polyglot'
use 'mattn/emmet-vim'
use 'turbio/bracey.vim'
use {
    "prettier/vim-prettier",
    run = "yarn install" 
}
use {
    'junegunn/fzf',
    run = './install --bin' 
}
use { 'ibhagwan/fzf-lua',
    requires = {
        'vijaymarupudi/nvim-fzf',
    }
}
use {
    "junegunn/fzf.vim",
}
use 'rust-lang/rust.vim'
-- use 'arzg/vim-rust-syntax-ext'
use { 
    "vim-syntastic/syntastic", 
}

end)
