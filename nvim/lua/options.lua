
local set = vim.opt

-- Stylish
set.number = true
set.relativenumber = true
set.termguicolors = true
set.wrap = false
vim.cmd('syntax enable')

-- Cursor
set.guicursor = ""
set.mouse = "a"

-- Tab and indentation
set.tabstop = 4
set.softtabstop = 4
set.expandtab = true
set.smartindent = true

-- General settings
set.swapfile = false
